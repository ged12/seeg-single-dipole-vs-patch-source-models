The goal of this study is to elucidate the correspondence between single-dipole and distributed source models for cortical generators of sEEG spikes. 

Methods and code:
    1) Create 800 patches of three cortex resolutions for modeling as distributed-source and centroid current source
        create_patches.m
            For each of three cortex resolutions,
            Create 400 patches on each hemisphere (2400 patches total).
            Pick 20 random locations on each cortex, and at each of those locations,
            build 20 patches of area 0.1, 0.5, 1, ... 9, 9.5 cm^2, iteratively
            expanding the patch each time. 
            
            For each patch, calculate the centroid, and create 4 centroid dipole 
            unit fields. The first centroid dipole is in the direction 
            of the linear combination of the patch face normal vectors, with the 
            areas of the triangluar faces as the weights for the linear combination. 
            The remaining three dipoles are also at the centroid but are in the
            x- y- and z- directions. 
            3200 total centroid dipole unit fields are created.

            For each patch, calculate the smallest distance from any vertex on the
            patch to any contact, the patch field surface triangulation, and the 
            equivalent centroid dipole field (node = centroid, field = linear combination
                of patch face normal vectors)


    2) Get forward solutions for all dipoles on cortex surface and centroids
       
           create_python_scripts_for_SCIRun_networks.m
                A. centroid dipoles
                    Create 9600 python scripts to create 9600 SCIRun networks to run on Duke Computing Cluster.
                    Centroid dipoles of unit strength corresponding to patch models, for each of 3 resolutions. 
                B. cortex dipoles for patch model
                    For each of three cortex resolutions,
                    Create python scripts for all elementary/face dipoles in cortex surface to get all single 
                    dipole forward solutions needed to create patch forward solutions. Each dipole current source is located at the 
                    centroid of one face on the cortex surface, oriented orthogonally. The SCIRun networks output 
                    a forward solution, a field of voltages at all points of a 2mm-spaced grid of the head.

                For each patch, run four current dipoles at the centroid:
                    one in the direction of the linear combination of patch face normals
                    one in each of the x-, y-, and z- directions 
                For each cortex resolution, run dipoles corresponsing to faces on cortex surface 
                    needed to create patches. 
                Run on the Duke Computing Cluster

            PatientForwardDipoleAllAutomationR_centroidmodel_HigherRes_ContactLocs.srn5 ,         
            PatientForwardDipoleAllAutomation_centroidmodel_HigherRes_ContactLocs.srn5
                Base SCIRun networks to copy with changes for every centroid dipole

            concatenate_centroid_model_voltages.m
                Concatenate all contact voltage fields from 9600 dipoles into 
                    left and right hemsphere true-direction, x-dir, y-dir, and z-dir files (8 total).
                    Each file is a 3x1 cell array that gives the data for all three cortex resolutons.
                and concatenate all forward solutions into the same storage structure.
                for each patch, there are four centroid dipoles (true direction, x-dir,
                y-dir, and z-dir). 


    3) Build patch forward solutions
        getPatchContactVoltages.m
            For each of three patch resolutions,
            Get patch model centroid voltages for centroid model patches.
            Calculate the whole patch forward solution by taking linear combinations 
            of the patch dipole forward solutions, scaling by the area of each 
            corresponding face on the cortical surface triangluation.
            Then, interpolate the forward solution (grid voltage) onto contact
            locations to get the contact voltages
            

    4) Test practical linearity of system
        test_practical_linearity.m 
            For each of three patch resolutions,
            Calculate the contact voltages of the centroid dipole in the
            direction of the linear combination of patch face normals using a linear
            combination of the contact voltages of the three orthogonal basis
            centroid dipoles. 
            Then, compare these linear combination contact voltages with the true 
            contact voltages given by the dipole that was originally run in that direction.
            To compare them, calculate the maximum voltage difference at any contact
            location

        plot_TestPracticalLinearity_MaxVoltDiff_vs_PatchArea.m 
            Plot the maximum voltage difference at a contact between the voltages of the true 
            direction centroid dipole and the equivalent linear combination dipole.


    5) Get Best Centroid Model
        get_best_centroid_contact_voltages.m 
            For all three patch resolutions,
            For every direction (every integer degree with spherical coordinates) of the centroid dipole current
            source, get the output contact voltages using linear combinations of the
            contact voltages due to the orthogonal basis centroid dipoles, and get
            the r-value (pearson correlation coefficient) of the linear combination
            centroid dipole contact voltages compared to the patch model contact 
            voltages
            The centroid dipole with the highest r-value is determined to be the best
            centroid dipole.


    6) Compare contact voltages from both centroid models to contact voltages from patch model
        plot_MaxVoltDiff_vs_Dist.m 
            Plot the maximum difference in voltage at contact locations between the
            patch model and both centroid models (simple and best) as a function of
            shortest distance from patch to any electrode. 
            With y- scale log or linear

        plot_MaxVoltDiff_vs_PatchArea.m 
            Plot the maximum difference in voltage at contact locations between the
            patch model and both centroid models (simple and best) as a function of
            patch area.
            With y- scale log or linear
            The max voltage difference of each set 40 of patches of similar area can also be
            viewed as a box plot.

        plot_RMS_vs_Dist.m 
            Plot the root mean squared error of contact locations between the
            patch model and both centroid models (simple and best) as a function of
            shortest distance from patch to any electrode
            With y- scale log or linear
        
        plot_RMS_vs_PatchArea.m 
            Plot the root mean squared error of voltage at contact locations between the
            patch model and both centroid models (simple and best) as a function of
            patch area.
            With y- scale log or linear
            The max voltage difference of each set 40 of patches of similar area can also be
            viewed as a box plot.

        plot_RMS_3D.m 
            Plot the RMS error in voltage at contact locations between the 
            patch model and both centroid models (simple and best) as a function of
            shortest distance from patch to any electrode and patch area.

        plot_MaxVoltDiff_3D.m 
            Plot the root mean squared error in voltage at contact locations between the 
            patch model and both centroid models (simple and best) as a function of
            shortest distance from patch to any electrode and patch area.

        
        












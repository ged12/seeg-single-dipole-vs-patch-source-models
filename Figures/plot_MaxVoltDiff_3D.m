%% Max Volt Diff of Contact Voltages vs. Patch Distance and Patch Area

% plot the maximum difference in voltage at contact locations between the
% patch model and both centroid models (simple and best) as a function of
% shortest distance from patch to any electrode

% **********************************************************************
% 10/22/20
% Grace Dessert
% **********************************************************************

centroid_model = 1; % 1 = simple model,  2 = best model
resolution = 1;
all_res = 0; %  1 == plot all resolutions together


path = '/Volumes/Seagate Backup Plus Drive/disc-signal-grill/CentroidModel/HigherRes/';
pathplot = '/Volumes/Seagate Backup Plus Drive/disc-signal-grill/CentroidModel/PublishAll/';


% LH 
% load patch contact voltages
load([path 'PatchContactVoltages_highRes.mat'],'patch_contact_voltages')
% load centroid true contact voltages
load([path 'CentroidModelTrueDirContactVoltages_highRes.mat'],'CentroidModelTrueDirContactVoltages'); 
% load centroid best dir contact voltages
load([path 'CentroidModelBestDirContactVoltages.mat'],'bestC_CVs'); 
% load patch areas and distances to electrode
load([path 'Patches_LH_all.mat'],'patchesLH')
patch_contact_voltages_LH = cell(3,1);
CentroidModelTrueDirContactVoltages_LH = cell(3,1);
bestC_CVs_LH = cell(3,1);
patch_areas_LH = cell(3,1);
dist_to_closest_contact_LH = cell(3,1);
for r = 1:3
    patch_contact_voltages_LH{r} = patch_contact_voltages{r};
    CentroidModelTrueDirContactVoltages_LH{r} = CentroidModelTrueDirContactVoltages{r};
    bestC_CVs_LH{r} = bestC_CVs{r};
    patch_areas_LH{r} = patchesLH{r}.areas;
    dist_to_closest_contact_LH{r} = patchesLH{r}.minDtoContact;
end

% RH load
% load patch contact voltages
load([path 'PatchContactVoltagesR_highRes.mat'],'patch_contact_voltages')
% load centroid true contact voltages
load([path 'CentroidModelTrueDirContactVoltagesR_highRes.mat'],'CentroidModelTrueDirContactVoltages'); 
% load centroid best dir contact voltages
load([path 'CentroidModelBestDirContactVoltagesR.mat'],'bestC_CVs'); 
% load patch areas and distances to electrode
load([path 'Patches_RH_all.mat'],'patchesRH')
patch_contact_voltages_RH = cell(3,1);
CentroidModelTrueDirContactVoltages_RH = cell(3,1);
bestC_CVs_RH = cell(3,1);
patch_areas_RH = cell(3,1);
dist_to_closest_contact_RH = cell(3,1);
for r = 1:3
    patch_contact_voltages_RH{r} = patch_contact_voltages{r};
    CentroidModelTrueDirContactVoltages_RH{r} = CentroidModelTrueDirContactVoltages{r};
    bestC_CVs_RH{r} = bestC_CVs{r};
    patch_areas_RH{r} = patchesRH{r}.areas;
    dist_to_closest_contact_RH{r} = patchesRH{r}.minDtoContact;
end

% combine LH and RH data
patch_areas = cell(3,1);
dist_to_closest_contact = cell(3,1);
for r  = 1:3
patch_contact_voltages{r} = [patch_contact_voltages_LH{r},patch_contact_voltages_RH{r}];
CentroidModelTrueDirContactVoltages{r} = [CentroidModelTrueDirContactVoltages_LH{r},CentroidModelTrueDirContactVoltages_RH{r}];
bestC_CVs{r} = [bestC_CVs_LH{r},bestC_CVs_RH{r}];
patch_areas{r} = [patch_areas_LH{r}',patch_areas_RH{r}'];
dist_to_closest_contact{r} = [dist_to_closest_contact_LH{r}',dist_to_closest_contact_RH{r}'];
end



rms = cell(3,1);
max_error = cell(3,1);
for r = 1:3
    % RMS (root mean squared) error is 
    rms{r} = zeros(1,800);
    max_error{r} = zeros(1,800);

    for patchI = 1:800
    patch_voltages = patch_contact_voltages{r}(:,patchI);
    if centroid_model == 1
    raw_centroid_CVs = CentroidModelTrueDirContactVoltages{r}(:,patchI);
    elseif centroid_model == 2
        raw_centroid_CVs = bestC_CVs{r}(:,patchI);
    end
    % get best dipole moment for centroid model (slope of linear regression)
    patchI;
    slope = raw_centroid_CVs \ patch_voltages;
    scaled_best_centroid_CVs = raw_centroid_CVs * slope;
    %plot(scaled_best_centroid_CVs,patch_voltages,'.')
    %pause(0.1)
    rms{r}(patchI) = sqrt(mean((scaled_best_centroid_CVs-patch_voltages).^2));
    max_error{r}(patchI) = max(abs(scaled_best_centroid_CVs-patch_voltages));

    end
end


%
%{
figure(6)
clf
hold on
colors = parula(2000);
passX = dist_to_closest_contact(max_error<=20);
passY = patch_areas(max_error<=20);
passZ = max_error(max_error<=80);

npassX = dist_to_closest_contact(max_error>=80);
npassY = patch_areas(max_error>=80);

npassZ = max_error(max_error>=80);

mpassX = dist_to_closest_contact(max_error<80 & max_error>20);
mpassY = patch_areas(max_error<80 & max_error>20);
mpassZ = max_error(max_error<80 & max_error>20);

color1 = colors(1,:);
color2 = colors(1650,:);

colorss = zeros(3,1000);
colorss(1,:) = linspace(colors(2,1),colors(9,1).^7,1000);
colorss(2,:) = linspace(colors(2,2),colors(9,2).^7,1000);
colorss(3,:) = linspace(colors(2,3),colors(9,3).^7,1000);
max_error_grid = linspace(min(mpassZ),max(mpassZ),1000);


c1 = zeros(3,length(mpassZ));

ok = parula(2000);
c1(1,:) = interp1(max_error_grid',ok(501:1500,1),mpassZ');
c1(2,:) = interp1(max_error_grid',ok(501:1500,2),mpassZ');
c1(3,:) = interp1(max_error_grid',ok(501:1500,3),mpassZ');

colormap(ok(1:1650,:))


% make sizes from 30 to 84
sizes = (mpassZ - min(mpassZ)).*((max(mpassZ)-min(mpassZ))/(84-30))+30;

scatter(npassX/10,npassY/100,84,color2,'filled','DisplayName','Voltage Diff >= 80 �V');

scatter(passX/10,passY/100,30,color1,'filled','DisplayName','Voltage Diff <= 20 �V');

for i = 1:length(mpassY)
scatter(mpassX(i)/10,mpassY(i)/100,sizes(i),mpassZ(i),'filled');
end
colorbar

legend('Voltage Diff >= 80 �V','Voltage Diff <= 20 �V')
xlabel('Shortest Distance to any Electrode (cm)')
ylabel('Patch Area (cm^2)')

if centroid_model == 1
title({'Max Contact Voltage Difference (�V)','Centroid Model: Simple'})
elseif centroid_model == 2
title({'Max Contact Voltage Difference (�V)','Centroid Model: Best'})
end

if centroid_model == 1 % simple model
    fileName=[pathplot 'Figures/MaxVoltageDifference_3D_Semilog_Simple.pdf'];
elseif centroid_model == 2 % best model
    fileName=[pathplot 'Figures/MaxVoltageDifference_3D_Surf_Semilog_Best.pdf'];
end

print(gcf,fileName,'-bestfit','-dpdf');

%}



% plot

figure(6)
clf
hold on
if all_res == 1
    dist_to_closest_contact = reshape(cell2mat(dist_to_closest_contact),1,800*3);
    max_error = reshape(cell2mat(max_error),1,800*3);
    patch_areas = reshape(cell2mat(patch_areas),1,800*3);
    
    colors = parula(2000);
    passX = dist_to_closest_contact(max_error<=20);
    passY = patch_areas(max_error<=20);
    passZ = max_error(max_error<=80);

    npassX = dist_to_closest_contact(max_error>=80);
    npassY = patch_areas(max_error>=80);

    npassZ = max_error(max_error>=80);

    mpassX = dist_to_closest_contact(max_error<80 & max_error>20);
    mpassY = patch_areas(max_error<80 & max_error>20);
    mpassZ = max_error(max_error<80 & max_error>20);

    color1 = colors(1,:);
    color2 = colors(1650,:);

    colorss = zeros(3,1000);
    colorss(1,:) = linspace(colors(2,1),colors(9,1).^7,1000);
    colorss(2,:) = linspace(colors(2,2),colors(9,2).^7,1000);
    colorss(3,:) = linspace(colors(2,3),colors(9,3).^7,1000);
    max_error_grid = linspace(min(mpassZ),max(mpassZ),1000);


    c1 = zeros(3,length(mpassZ));

    ok = parula(2000);
    c1(1,:) = interp1(max_error_grid',ok(501:1500,1),mpassZ');
    c1(2,:) = interp1(max_error_grid',ok(501:1500,2),mpassZ');
    c1(3,:) = interp1(max_error_grid',ok(501:1500,3),mpassZ');

    colormap(ok(1:1650,:))

    % make sizes from 30 to 84
    sizes = (mpassZ - min(mpassZ))./max((mpassZ - min(mpassZ)))*(44-20)+20;

    scatter(npassX/10,npassY/100,44,color2,'filled','DisplayName','Voltage Diff >= 80 �V');
    scatter(passX/10,passY/100,20,color1,'filled','DisplayName','Voltage Diff <= 20 �V');

    for i = 1:length(mpassY)
    scatter(mpassX(i)/10,mpassY(i)/100,sizes(i),mpassZ(i),'filled');
    end

    colorbar
    legend('Voltage Diff >= 80 �V','Voltage Diff <= 20 �V')
    xlabel('Shortest Distance to any Electrode (cm)')
    ylabel('Patch Area (cm^2)')

    if centroid_model == 1
        title({'Max Contact Voltage Difference for all resolutions','Centroid Model: Simple'})
    elseif centroid_model == 2
        title({'Max Contact Voltage Difference for all resolutions','Centroid Model: Best'})
    end


    if centroid_model == 1 % simple model 
            fileName=[pathplot 'Figures/MaxVoltageDifference_3D_Semilog_Simple_allRes.pdf'];

    elseif centroid_model == 2 % best model
            fileName=[pathplot 'Figures/MaxVoltageDifference_3D_Semilog_Best_allRes.pdf'];

    end
    print(gcf,fileName,'-bestfit','-dpdf');
    
    
else
    colors = parula(2000);
    passX = dist_to_closest_contact{resolution}(max_error{resolution}<=20);
    passY = patch_areas{resolution}(max_error{resolution}<=20);
    passZ = max_error{resolution}(max_error{resolution}<=80);

    npassX = dist_to_closest_contact{resolution}(max_error{resolution}>=80);
    npassY = patch_areas{resolution}(max_error{resolution}>=80);

    npassZ = max_error{resolution}(max_error{resolution}>=80);

    mpassX = dist_to_closest_contact{resolution}(max_error{resolution}<80 & max_error{resolution}>20);
    mpassY = patch_areas{resolution}(max_error{resolution}<80 & max_error{resolution}>20);
    mpassZ = max_error{resolution}(max_error{resolution}<80 & max_error{resolution}>20);

    color1 = colors(1,:);
    color2 = colors(1650,:);

    colorss = zeros(3,1000);
    colorss(1,:) = linspace(colors(2,1),colors(9,1).^7,1000);
    colorss(2,:) = linspace(colors(2,2),colors(9,2).^7,1000);
    colorss(3,:) = linspace(colors(2,3),colors(9,3).^7,1000);
    max_error_grid = linspace(min(mpassZ),max(mpassZ),1000);


    c1 = zeros(3,length(mpassZ));

    ok = parula(2000);
    c1(1,:) = interp1(max_error_grid',ok(501:1500,1),mpassZ');
    c1(2,:) = interp1(max_error_grid',ok(501:1500,2),mpassZ');
    c1(3,:) = interp1(max_error_grid',ok(501:1500,3),mpassZ');

    colormap(ok(1:1650,:))

    % make sizes from 30 to 84
    sizes = (mpassZ - min(mpassZ)).*((max(mpassZ)-min(mpassZ))/(84-30))+30;

    scatter(npassX/10,npassY/100,84,color2,'filled','DisplayName','Voltage Diff >= 80 �V');
    scatter(passX/10,passY/100,30,color1,'filled','DisplayName','Voltage Diff <= 20 �V');

    for i = 1:length(mpassY)
    scatter(mpassX(i)/10,mpassY(i)/100,sizes(i),mpassZ(i),'filled');
    end

    colorbar
    legend('Voltage Diff >= 80 �V','Voltage Diff <= 20 �V')
    xlabel('Shortest Distance to any Electrode (cm)')
    ylabel('Patch Area (cm^2)')

    
    if centroid_model == 1 && all_res~=1
    title({['Max Contact Voltage Difference for resolution ' num2str(resolution)],'Centroid Model: Simple'})
        fileName=[pathplot 'Figures/MaxVoltageDifference_3D_Semilog_Simple_Res'  num2str(resolution) '.pdf'];
    elseif centroid_model == 2 && all_res~=1
    title({['Max Contact Voltage Difference for resolution ' num2str(resolution)],'Centroid Model: Best'})
    fileName=[pathplot 'Figures/MaxVoltageDifference_3D_Semilog_Best_Res'  num2str(resolution) '.pdf'];

    end
end

if centroid_model == 1 && all_res==1% simple model
        fileName=[pathplot 'Figures/MaxVoltageDifference_3D_Semilog_Simple_allRes.pdf'];

elseif centroid_model == 2 && all_res==1% best model
        fileName=[pathplot 'Figures/MaxVoltageDifference_3D_Semilog_Best_allRes.pdf'];

end
    

print(gcf,fileName,'-bestfit','-dpdf');

%% create 800 patches

% ************************************************************************
% 3/15/21
% Grace Dessert
% ************************************************************************

% using three resolutions of the cortex surface,
% create 400 patches on each hemisphere
% pick 20 random locations on each cortex, and at each of those locations,
% build 20 patches of area 0.1, 0.5, 1, ... 9, 9.5 cm^2, iteratively
% expanding the patch each time. 
% 
% for each patch, calculate the centroid, and create 4 centroid dipole 
% fields. The first centroid dipole is in the direction 
% of the linear combination of the patch face normal vectors, with the 
% areas of the triangluar faces as the weights for the linear combination. 
% The remaining three dipoles are also at the centroid but are in the
% x- y- and z- directions, again with strength 20nAm. 
% 3200 total centroid dipole fields are created.
%
% for each patch, calculate the smallest distance from any vertex on the
% patch to any contact.

% input:
%           lh_white_145k.stl, rh_white_145k.stl
%               two roughly 145k element triangular surface meshes of the 
%               outer cortex
%           lh_white_original_291k.stl, rh_white_original_283k.stl
%               two highest resolution triangular surface meshes of the 
%               outer cortex
%           lh_white_20k.stl, rh_white_20k.stl
%               two roughly 20k element triangular surface meshes of the 
%               outer cortex
%           total_num_patches = 400
%               the total number of patches per hemisphere per resolution
%                   to create. 
%               but, use less patches for highest resolution

% output:

%           lh_white_3res_areas.mat, areasLH_all ; rh_white_3res_areas.mat, areasRH_all
%               the areas of all faces in the tri surface meshes

%           lh_white_3res_normals.mat, normalsLH_all ; rh_white_3res_normals.mat , normalsRH_all
%               the normal vectors of all faces in the tri surface meshes
%               for the three resolutions

%           CentroidFieldFourDirectionsForEach_LH.mat,centroid  ;  CentroidFieldFourDirectionsForEach_RH.mat,centroid
%               1x1 struct with 2 fields:   .field is a 3x1600 double of the centroid dipole moments (4 for each centroid; 20nAm each)
%                                           .node is a 3x1600 double of the centroid locations (4 of each centroid location)

%           Patches_LH_all.mat , patchesLH  ;  Patches_RH_all.mat, patchesRH
%               1x3 cell array. each cell is a
%               structure with five fields
%                   .dipoles gives the dipole indices in the corresponding
%                   cortex surface that make up each patch 
%                   .areas 400x1 double of the area of each patch in mm^2
%                   .minDtoContact 400x1 double of the minimum distance 
%                       in mm of each patch to any contact
%                   .centroids gives the 400 single dipole fields (.node
%                       and .field) of the corresponding patch model.
%                       .node gives the location of the centroid, and
%                       .field gives the linear combination direction of
%                       the net dipole moment of the patch model
%                   .patchFields gives the triangulation surface of each 
%                       patch model

%           centroidDipoles_LH_centroidModel_highRes.mat, centroidDipoles_LH_centroidModel_highRes  ;  centroidDipoles_RH_centroidModel_highRes.mat, centroidDipoles_RH_centroidModel_highRes
%               for 145k cortex surface, this is the
%               field of 400 equivalent centroid dipoles for each hemisphere.
%               .node gives the locations of the centroids 
%               .field gives the linear combination of patch normal vectors

%           centroidDipoles_LH_centroidModel_fullRes.mat, centroidDipoles_LH_centroidModel_fullRes  ;  centroidDipoles_RH_centroidModel_fullRes.mat, centroidDipoles_RH_centroidModel_fullRes
%               for the full resolution cortex surface, this is the
%               field of 400 equivalent centroid dipoles for each hemisphere.
%               .node gives the locations of the centroids 
%               .field gives the linear combination of patch normal vectors

%           centroidDipoles_withBasisDirs_LH_centroidModel_highRes_all.mat, centroid ; centroidDipoles_withBasisDirs_RH_centroidModel_highRes_all.mat','centroid
%               for all three resolutions, this is the
%               field of 1600 centroid dipoles for each hemisphere.
%               four dipoles for each patch model, 1 in the direction 
%               of the linear combination of the patch face normal vectors, with the 
%               areas of the triangluar faces as the weights for the linear combination. 
%               The remaining three dipoles are also at the centroid but are in the
%               x- y- and z- directions.
%               .node gives the locations of the dipoles 
%               .field gives the directioins of the dipoles

%           CentroidField_LH$.mat, centroidDipole  ; CentroidField_RH$.mat, centroidDipole
%               this is the centroidDipoles_withBasisDirs_L/RH_centroidModel_highRes_all
%               field of centroids, with each single dipole field saved
%               separately. $ = 1:4800 for each hemisphere
%                   $1:1600 = second resolution (145k)
%                   $1601:3200 = first/lowest resolution (20k)
%                   $3201:4800 = third/highst resolution (~280k)

%           dipoles_LH_centroidModel_highRes.mat, dipoles_LH_centroidModel_highRes ; dipoles_RH_centroidModel_highRes.mat, dipoles_RH_centroidModel_highRes
%               field of cortex face dipoles needed for these patches
%               .node gives the location of the dipoles 
%               .field gives the direction of the dipoles (normals)

%           dipoleIndsLH_all.mat, dipoleIndsLH ; dipoleIndsLH_all.mat, dipoleIndsRH
%               set of indices of dipoles in 145k surfaces that need to be 
%               run for these patches, for all three resolutions
% ************************************************************************

local_or_cluster = 1;

% load surfaces
stl_lh_145k = stlread('/Volumes/Seagate Backup Plus Drive/disc-signal-grill/CentroidModel/HigherRes/lh_white_145k.stl');
stl_rh_145k = stlread('/Volumes/Seagate Backup Plus Drive/disc-signal-grill/CentroidModel/HigherRes/rh_white_145k.stl');
stl_lh_full = stlread('/Volumes/Seagate Backup Plus Drive/disc-signal-grill/CentroidModel/PatchResolutionTesting/CortexSurfacesGood/lh_white_original_291k.stl');
stl_rh_full = stlread('/Volumes/Seagate Backup Plus Drive/disc-signal-grill/CentroidModel/PatchResolutionTesting/CortexSurfacesGood/rh_white_original_283k.stl');
stl_lh_origial_20k = stlread('/Volumes/Seagate Backup Plus Drive/disc-signal-grill/lh_white_20k.stl');
stl_rh_origial_20k = stlread('/Volumes/Seagate Backup Plus Drive/disc-signal-grill/rh_white_20k.stl');

% create face areas and normals
%{
normalsLH_all = cell(3,1); % highest res = #1, 145k = #2, 20k = #3
areasLH_all = cell(3,1); % highest res = #1, 145k = #2, 20k = #3
normalsRH_all = cell(3,1); % highest res = #1, 145k = #2, 20k = #3
areasRH_all = cell(3,1); % highest res = #1, 145k = #2, 20k = #3

normalsLH_all{1} = get_cortex_normals(stl_lh_full);
areasLH_all{1} = get_cortex_areas(stl_lh_full);
normalsRH_all{1} = get_cortex_normals(stl_rh_full);
areasRH_all{1} = get_cortex_areas(stl_rh_full);

normalsLH_all{3} = get_cortex_normals(stl_lh_origial_20k);
areasLH_all{3} = get_cortex_areas(stl_lh_origial_20k);
normalsRH_all{3} = get_cortex_normals(stl_rh_origial_20k);
areasRH_all{3} = get_cortex_areas(stl_rh_origial_20k);

load('/Volumes/Seagate Backup Plus Drive/disc-signal-grill/CentroidModel/HigherRes/lh_white_145k_normals.mat','normalsLH');
normalsLH_all{2} = normalsLH;
load('/Volumes/Seagate Backup Plus Drive/disc-signal-grill/CentroidModel/HigherRes/lh_white_145k_areas.mat','areasLH');
areasLH_all{2} = areasLH;
load('/Volumes/Seagate Backup Plus Drive/disc-signal-grill/CentroidModel/HigherRes/rh_white_145k_normals.mat','normalsRH');
normalsRH_all{2} = normalsRH;
load('/Volumes/Seagate Backup Plus Drive/disc-signal-grill/CentroidModel/HigherRes/rh_white_145k_areas.mat','areasRH');
areasRH_all{2} = areasRH;

% save areas and normals
save('/Volumes/Seagate Backup Plus Drive/disc-signal-grill/CentroidModel/PatchResolutionTesting/ErrorProfileData/lh_white_3res_normals.mat','normalsLH_all');
save('/Volumes/Seagate Backup Plus Drive/disc-signal-grill/CentroidModel/PatchResolutionTesting/ErrorProfileData/lh_white_3res_areas.mat','areasLH_all');
save('/Volumes/Seagate Backup Plus Drive/disc-signal-grill/CentroidModel/PatchResolutionTesting/ErrorProfileData/rh_white_3res_normals.mat','normalsRH_all');
save('/Volumes/Seagate Backup Plus Drive/disc-signal-grill/CentroidModel/PatchResolutionTesting/ErrorProfileData/rh_white_3res_areas.mat','areasRH_all');
%}

% load areas and normals
load('/Volumes/Seagate Backup Plus Drive/disc-signal-grill/CentroidModel/PatchResolutionTesting/ErrorProfileData/lh_white_3res_normals.mat','normalsLH_all');
load('/Volumes/Seagate Backup Plus Drive/disc-signal-grill/CentroidModel/PatchResolutionTesting/ErrorProfileData/lh_white_3res_areas.mat','areasLH_all');
load('/Volumes/Seagate Backup Plus Drive/disc-signal-grill/CentroidModel/PatchResolutionTesting/ErrorProfileData/rh_white_3res_normals.mat','normalsRH_all');
load('/Volumes/Seagate Backup Plus Drive/disc-signal-grill/CentroidModel/PatchResolutionTesting/ErrorProfileData/rh_white_3res_areas.mat','areasRH_all');


%% make patches
% for each resolution

% LH
% start with 145k cortex surface
areasLH = areasLH_all{2};
total_num_patches = 400; % 400 on each hemisphere
patch_areas_target = reshape(repmat([0.1,[0.5:0.5:9.5]]',[20,1]),[400,1])*100; %  10 patches of each area. areas = 0.1,0.5,1.0,1.5,2,... 9.5 cm^2 . 400 patches total.
rng(9)
dipoles_toStartAt = reshape(repmat(randi(length(areasLH),1,20),[20,1]),[400,1]); % 20 faces to start build LH patches at
% get coordinates from dipole start, which are consistent across all three
% resolutions
coordinates_toStart_At_LH = normalsLH_all{2}.node(:,dipoles_toStartAt);


% initialize patch info storage variables
% patchesLH is a 1x3 cell array of all patch data for each of 3 resolutions
%   .dipoles is a 400x_ matrix where each row gives the set of all dipole indices that make up this patch
%   in given cortex resolution and given area
%   .areas is 400x1 matrix of areas of all 20 patch locations for all 20 areas
%   .centroid is field (.node and .field) of centoid location and directon
%   for all 400 patches (each field is 3x400 matrix)
clear patchesLH
patchesLH{1}.dipoles = zeros(total_num_patches,10); % each row gives the indices of faces that make up that patch  
patchesLH{1}.areas = zeros(total_num_patches,1); % in mm^2
patchesLH{1}.centroids.field = zeros(3,total_num_patches); % direction
patchesLH{1}.centroids.node = zeros(3,total_num_patches); % position / coord / location
patchesLH{1}.patchFields = cell(total_num_patches,1);
patchesLH{2} = patchesLH{1};
patchesLH{3} = patchesLH{1};


dipolesLHtoRun = cell(3,1); % store all dipoles that make up these patches

for i = 1:total_num_patches
    i
    coord_start = coordinates_toStart_At_LH(:,i);
    area_patch = patch_areas_target(i);
    % res full
    [patch,total_area,patchfield,centroid,netDipole] = make_patch_coord(stl_lh_full,normalsLH_all{1},areasLH_all{1},coord_start,area_patch,1,1);
    patchesLH{1}.dipoles(i,1:length(patch)) = patch;
    patchesLH{1}.areas(i) = total_area;
    patchesLH{1}.centroids.node(:,i) = centroid;
    patchesLH{1}.centroids.field(:,i) = netDipole;
    dipolesLHtoRun{1} = [dipolesLHtoRun{1};patch];
    patchesLH{1}.patchFields{i} = patchfield;
    % res 145k
    [patch,total_area,patchfield,centroid,netDipole] = make_patch_coord(stl_lh_145k,normalsLH_all{2},areasLH_all{2},coord_start,area_patch,1,1);
    patchesLH{2}.dipoles(i,1:length(patch)) = patch;
    patchesLH{2}.areas(i) = total_area;
    patchesLH{2}.centroids.node(:,i) = centroid;
    patchesLH{2}.centroids.field(:,i) = netDipole;
    dipolesLHtoRun{2} = [dipolesLHtoRun{2};patch];
    patchesLH{2}.patchFields{i} = patchfield;
    % res 30k
    [patch,total_area,patchfield,centroid,netDipole] = make_patch_coord(stl_lh_origial_20k,normalsLH_all{3},areasLH_all{3},coord_start,area_patch,1,1);
    patchesLH{3}.dipoles(i,1:length(patch)) = patch;
    patchesLH{3}.areas(i) = total_area;
    patchesLH{3}.centroids.node(:,i) = centroid;
    patchesLH{3}.centroids.field(:,i) = netDipole;
    dipolesLHtoRun{3} = [dipolesLHtoRun{3};patch];
    patchesLH{3}.patchFields{i} = patchfield;
end
%
% get all dipole indices and fields I need to run
% get unique dipoles
dipoleIndsLH = cell(3,1);
dipoles_LH_centroidModel = cell(3,1);
for r = 1:3
dipoleIndsLH{r} = unique(dipolesLHtoRun{r});
dipoles_LH_centroidModel{r}.node  = normalsLH_all{r}.node(:,dipoleIndsLH{r})+[3;0;68]; % shift normals to put into correct head coordinates
dipoles_LH_centroidModel{r}.field = normalsLH_all{r}.field(:,dipoleIndsLH{r});
end
%
% save indices of dipoles in each surface that we need to construct patch
% forward solutions
save('/Volumes/Seagate Backup Plus Drive/disc-signal-grill/CentroidModel/HigherRes/dipoleIndsLH_all.mat','dipoleIndsLH')

% save 145k resolution surface dipoles to run
dipoles_LH_centroidModel_highRes = dipoles_LH_centroidModel{2};
save('/Volumes/Seagate Backup Plus Drive/disc-signal-grill/CentroidModel/HigherRes/dipoles_LH_centroidModel_highRes.mat','dipoles_LH_centroidModel_highRes')

% save full resolution surface dipoles to run
dipoles_LH_centroidModel_fullRes = dipoles_LH_centroidModel{1};
save('/Volumes/Seagate Backup Plus Drive/disc-signal-grill/CentroidModel/HigherRes/dipoles_LH_centroidModel_fullRes.mat','dipoles_LH_centroidModel_fullRes')

% 20k resolution surface dipoles have already been run. don't need to save
% these. 

% visualize patches
stlPlot(stl_lh_145k.Points,stl_lh_145k.ConnectivityList,dipoleIndsLH{2},'Left Hemisphere')

%% make patches RH

% start with 145k cortex surface
areasRH = areasRH_all{2};
total_num_patches = 400; % 400 on each hemisphere for each resolution
rng(8)
dipoles_toStartAt = reshape(repmat(randi(length(areasRH),1,20),[20,1]),[400,1]); % 20 faces to start build LH patches at
% get coordinates from dipole start, which are consistent across all three
% resolutions
coordinates_toStart_At_RH = normalsRH_all{2}.node(:,dipoles_toStartAt);

% initialize patch info storage variables
% patchesLH is a 1x3 cell array of all patch data for each of 3 resolutions
%   .dipoles is a 400x_ matrix where each row gives the set of all dipole indices that make up this patch
%   in given cortex resolution and given area
%   .areas is 400x1 matrix of areas of all 20 patch locations for all 20 areas
%   .centroid is field (.node and .field) of centoid location and directon
%   for all 400 patches (each field is 3x400 matrix)
clear patchesRH
patchesRH{1}.dipoles = zeros(total_num_patches,10); % each row gives the indices of faces that make up that patch  
patchesRH{1}.areas = zeros(total_num_patches,1); % in mm^2
patchesRH{1}.centroids.field = zeros(3,total_num_patches); % direction
patchesRH{1}.centroids.node = zeros(3,total_num_patches); % position / coord / location
patchesRH{1}.patchFields = cell(total_num_patches,1);
patchesRH{2} = patchesRH{1};
patchesRH{3} = patchesRH{1};

dipolesRHtoRun = cell(3,1); % store all dipoles that make up these patches
for i = 1:total_num_patches
    i
    coord_start = coordinates_toStart_At_RH(:,i);
    
    area_patch = patch_areas_target(i);
    % res full
    [patch,total_area,patchfield,centroid,netDipole] = make_patch_coord(stl_rh_full,normalsRH_all{1},areasRH_all{1},coord_start,area_patch,2,1);
    patchesRH{1}.dipoles(i,1:length(patch)) = patch;
    patchesRH{1}.areas(i) = total_area;
    patchesRH{1}.centroids.node(:,i) = centroid;
    patchesRH{1}.centroids.field(:,i) = netDipole;
    dipolesRHtoRun{1} = [dipolesRHtoRun{1};patch];
    patchesRH{1}.patchFields{i} = patchfield;
    % res 145k
    [patch,total_area,patchfield,centroid,netDipole] = make_patch_coord(stl_rh_145k,normalsRH_all{2},areasRH_all{2},coord_start,area_patch,2,1);
    patchesRH{2}.dipoles(i,1:length(patch)) = patch;
    patchesRH{2}.areas(i) = total_area;
    patchesRH{2}.centroids.node(:,i) = centroid;
    patchesRH{2}.centroids.field(:,i) = netDipole;
    dipolesRHtoRun{2} = [dipolesRHtoRun{2};patch];
    patchesRH{2}.patchFields{i} = patchfield;
    % res 30k
    [patch,total_area,patchfield,centroid,netDipole] = make_patch_coord(stl_rh_origial_20k,normalsRH_all{3},areasRH_all{3},coord_start,area_patch,2,1);
    patchesRH{3}.dipoles(i,1:length(patch)) = patch;
    patchesRH{3}.areas(i) = total_area;
    patchesRH{3}.centroids.node(:,i) = centroid;
    patchesRH{3}.centroids.field(:,i) = netDipole;
    dipolesRHtoRun{3} = [dipolesRHtoRun{3};patch];
    patchesRH{3}.patchFields{i} = patchfield;

end
%
% get all dipole indices and fields I need to run
% get unique dipoles
dipoleIndsRH = cell(3,1);
dipoles_RH_centroidModel = cell(3,1);
for r = 1:3
dipoleIndsRH{r} = unique(dipolesRHtoRun{r});
dipoles_RH_centroidModel{r}.node  = normalsRH_all{r}.node(:,dipoleIndsRH{r})+[3;0;70]; % shift normals to put into correct head coordinates
dipoles_RH_centroidModel{r}.field = normalsRH_all{r}.field(:,dipoleIndsRH{r});
end

%
% save indices of dipoles in each surface that we need to construct patch
% forward solutions
save('/Volumes/Seagate Backup Plus Drive/disc-signal-grill/CentroidModel/HigherRes/dipoleIndsRH_all.mat','dipoleIndsRH')

% save 145k resolution surface dipoles to run
dipoles_RH_centroidModel_highRes = dipoles_RH_centroidModel{2};
save('/Volumes/Seagate Backup Plus Drive/disc-signal-grill/CentroidModel/HigherRes/dipoles_RH_centroidModel_highRes.mat','dipoles_RH_centroidModel_highRes')

% save full resolution surface dipoles to run
dipoles_RH_centroidModel_fullRes = dipoles_RH_centroidModel{1};
save('/Volumes/Seagate Backup Plus Drive/disc-signal-grill/CentroidModel/HigherRes/dipoles_RH_centroidModel_fullRes.mat','dipoles_RH_centroidModel_fullRes')

% 20k resolution surface dipoles have already been run. don't need to save
% these. 

% visualize patches
stlPlot(stl_rh_145k.Points,stl_rh_145k.ConnectivityList,dipoleIndsRH{2},'Right Hemisphere')


%% get min distance of each patch to any contact

% LH
% get patch field
side = 1;
% get contact locations
load('/Users/dessertgrace/Documents/project_seeg_discernable_signal_grill_-selected/ContactLocations.mat','tetmesh');

for r = 1:3
    patchesLH{r}.minDtoContact = zeros(total_num_patches,1);
    if r ==1
        h.vertices  = stl_lh_full.Points;
        h.faces = stl_lh_full.ConnectivityList;
    elseif r == 2
        h.vertices  = stl_lh_145k.Points;
        h.faces = stl_lh_145k.ConnectivityList;
    else
        h.vertices  = stl_lh_origial_20k.Points;
        h.faces = stl_lh_origial_20k.ConnectivityList;
    end
    for patchI = 1:total_num_patches
        % get patch field
        patchfield = patchesLH{r}.patchFields{patchI};
        Points = patchfield.Points;
        %for every contact location, get distance to every point in patch field
        dist_to_closest_contact_all = zeros(length(tetmesh.node),size(Points,1));
        for contact=1:length(tetmesh.node)
            dist_to_closest_contact_all(contact,:) = min(sqrt(sum((Points'-tetmesh.node(:,contact)).^2,1)));
        end
        patchesLH{r}.minDtoContact(patchI) = min(min(dist_to_closest_contact_all));
        min(min(dist_to_closest_contact_all))
    end
end
%

%RH
side = 2;
for r = 1:3
    patchesRH{r}.minDtoContact = zeros(total_num_patches,1);
    if r ==1
        h.vertices  = stl_rh_full.Points;
        h.faces = stl_rh_full.ConnectivityList;
    elseif r == 2
        h.vertices  = stl_rh_145k.Points;
        h.faces = stl_rh_145k.ConnectivityList;
    else
        h.vertices  = stl_rh_origial_20k.Points;
        h.faces = stl_rh_origial_20k.ConnectivityList;
    end
    for patchI = 1:total_num_patches
        % get patch field
        patchfield = patchesRH{r}.patchFields{patchI};
        Points = patchfield.Points;
        %for every contact location, get distance to every point in patch field
        dist_to_closest_contact_all = zeros(length(tetmesh.node),size(Points,1));
        for contact=1:length(tetmesh.node)
            dist_to_closest_contact_all(contact,:) = min(sqrt(sum((Points'-tetmesh.node(:,contact)).^2,1)));
        end
        patchesRH{r}.minDtoContact(patchI) = min(min(dist_to_closest_contact_all));
    end
end
%
save('/Volumes/Seagate Backup Plus Drive/disc-signal-grill/CentroidModel/HigherRes/Patches_LH_all.mat','patchesLH')
save('/Volumes/Seagate Backup Plus Drive/disc-signal-grill/CentroidModel/HigherRes/Patches_RH_all.mat','patchesRH')

%%
% examine spatial spread
total_num_patches = 400;
r = 3;
figure(1)
clf
hold on
title('Min Dist from patch to contact for error profile (20 patches)')
xlabel('distance (mm)')
scatter(patchesRH{r}.minDtoContact,ones(1,total_num_patches))
scatter(patchesLH{r}.minDtoContact,ones(1,total_num_patches))

%% get centroid dipoles and save individually
% for each patch, run equivalent centroid dipole
% in the true direction (linear comb. of patch face normals) and 
%   in the three basis directions (x, y, z). 

% RH
res = [2,1,3]; % go in this order so first 1600 already run have the same indices
clear centroid
centroid = cell(1,3);
for rInd = 1:3
    r = res(rInd);
    total_num_patches = 400;
    centroid{r}.field = zeros(3,total_num_patches*4); % direction
    centroid{r}.node = zeros(3,total_num_patches*4); % position 
    for i=1:400
        centroid{r}.node(:,(i-1)*4+1:i*4)=repmat(patchesLH{r}.centroid.node(:,i),1,4);
        centroid{r}.field(:,(i-1)*4+1)=patchesLH{r}.centroid.field(:,i);
        centroid{r}.field(:,(i-1)*4+2)=[1;0;0];
        centroid{r}.field(:,(i-1)*4+3)=[0;1;0];
        centroid{r}.field(:,(i-1)*4+4)=[0;0;1];
    end

    % split field of all centroid dipoles into individual dipole fields for each network to load and run
    inds_Save = (rInd-1)*1600+1:rInd*1600; % index of this centroid in all centroids for all resolutions. from 1 to 400*4*3 = 4800. each resolution has 1600 indices/centroids
    for i=1:total_num_patches*4
        centroidDipole.node = centroid{r}.node(:,i);
        centroidDipole.field = centroid{r}.field(:,i);
        indSave = inds_Save(i);
        save(['/Volumes/Seagate Backup Plus Drive/disc-signal-grill/CentroidModel/HigherRes/CentroidDipoleSingleFields_LH/CentroidField_LH' num2str(indSave) '.mat'],'centroidDipole');
    end
    
end

save('/Volumes/Seagate Backup Plus Drive/disc-signal-grill/CentroidModel/HigherRes/centroidDipoles_withBasisDirs_LH_centroidModel_highRes_all.mat','centroid')

%%

%save('/Volumes/Seagate Backup Plus Drive/disc-signal-grill/CentroidModel/HigherRes/centroidDipoles_withBasisDirs_RH_centroidModel_highRes.mat','centroid')

% RH
res = [2,1,3]; % go in this order so first 1600 already run have the same indices
clear centroid
centroid = cell(1,3);
for rInd = 1:3
    r = res(rInd);
    total_num_patches = 400;
    centroid{r}.field = zeros(3,total_num_patches*4); % direction
    centroid{r}.node = zeros(3,total_num_patches*4); % position 
    for i=1:400
        centroid{r}.node(:,(i-1)*4+1:i*4)=repmat(patchesRH{r}.centroids.node(:,i),1,4);
        centroid{r}.field(:,(i-1)*4+1)=patchesRH{r}.centroids.field(:,i);
        centroid{r}.field(:,(i-1)*4+2)=[1;0;0];
        centroid{r}.field(:,(i-1)*4+3)=[0;1;0];
        centroid{r}.field(:,(i-1)*4+4)=[0;0;1];
    end

    % split field of all centroid dipoles into individual dipole fields for each network to load and run
    inds_Save = (rInd-1)*1600+1:rInd*1600; % index of this centroid in all centroids for all resolutions. from 1 to 400*4*3 = 4800. each resolution has 1600 indices/centroids
    for i=1:total_num_patches*4
        centroidDipole.node = centroid{r}.node(:,i);
        centroidDipole.field = centroid{r}.field(:,i);
        indSave = inds_Save(i);
        save(['/Volumes/Seagate Backup Plus Drive/disc-signal-grill/CentroidModel/HigherRes/CentroidDipoleSingleFields_RH/CentroidField_RH' num2str(indSave) '.mat'],'centroidDipole');
    end
    
end

save('/Volumes/Seagate Backup Plus Drive/disc-signal-grill/CentroidModel/HigherRes/centroidDipoles_withBasisDirs_RH_centroidModel_highRes_all.mat','centroid')





function [patch,total_area,patchfield,centroid,netDipole] = make_patch_coord(FV,normals,areas,coordinate,atarget,side,cdmd)

% adapted from make_patch_startface
% make patch starting from the face that is closest to the given
% coordinate
%
% 1) find face in cortex surface whose normal is closest to the coordinate
% given
% 2) start with this face in patch
% 3) iteratively expand until patch is at least as large as atarget
% 4) calculate patchfield for visualization

% find closest face (ctr of face given by loc of normal)
if size(coordinate,1)==1
    coordinate = coordinate';
end
distances = sum(sqrt((normals.node-coordinate).^2),1);
sface = find(distances==min(distances));
sface = sface(1);

h.faces = FV.ConnectivityList;
h.vertices = FV.Points;

outer_vertices = [];
total_area = areas(sface);
patch = sface;
for y = 1:3
    outer_vertices = [outer_vertices,h.faces(sface,y)];
end

% iteratively expand patch by outervertices (one vertex at a time)
start = 1;
while total_area < atarget
    disconnected = 0;
     for v = outer_vertices
        if total_area >= atarget
            break
        end
        [ia,~] = find(h.faces==v);
        ia = unique(ia);
        ia(ismember(ia,patch)==1)=[];
        if ~isempty(ia) 
            for f = transpose(ia)
                if total_area >= atarget
                    break
                end
                patch = [patch;f];
                total_area = total_area + areas(f);
                for coord = 1:3
                    if ismember(h.faces(f,coord),outer_vertices) == 0
                        outer_vertices = [outer_vertices,h.faces(f,coord)];
                        start = 0;
                    end
                end
            end 
        else
            disconnected = disconnected + 1;
        end
        outer_vertices(outer_vertices==v)=[];
     end
     if disconnected == 3 && start == 1
         disconnected = "first dipole is disconnected! no patch can be made."
         break
     end
end

% make patch triangulation
Points = [];
ConnectivityList = zeros(length(patch),3);
point_index = 1;
for x = 1:length(patch) % x = patch number
    for c = 1:3
        % h.faces(patch(x),c) = vertex coord index (in h.vertices)
        a = h.vertices(h.faces(patch(x),c),:); % = new vertex coords
        if side == 1
            a(3) = a(3) + 68;
            a(1) = a(1) + 3;
        elseif side == 2
            a(3) = a(3) + 70;
            a(1) = a(1) + 3;
        end
        Points = [Points;a];
        ConnectivityList(x,c) = point_index;
        point_index = point_index + 1;
    end
end
patchfield = triangulation(ConnectivityList,Points);    
    
% calc centroid and netDipole
acc_mass = 0;
acc_area = 0;
b = [0;0;0];
for index = 1:length(patch)
    ctr = normals.node(:,patch(index));
    dipoleM = normals.field(:,patch(index))*areas(patch(index))*cdmd;
    b = b + dipoleM;
    acc_mass = acc_mass + (ctr * areas(patch(index)));
    acc_area = acc_area + areas(patch(index));
end 
a = acc_mass ./ acc_area;
if side == 1
    a(3) = a(3) + 68;
    a(1) = a(1) + 3;
elseif side == 2
    a(3) = a(3) + 70;
    a(1) = a(1) + 3;
end

centroid = a;
netDipole = b;

end

%%% not used ***** same as make_patch_coord but starts from a given dipole
%%% index rather than a coordinate location. %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [patch,total_area,patchfield,centroid,netDipole] = make_patch(FV,normals,areas,sface,atarget,side,cdmd)

% ********************************
% make_patch(FV,normals,areas,sface,atarget,side,cdmd) returns an array containing the
% indeces of faces in the cortical surface triangulation of the side of 
% interest (1:left, 2:right), the total area of this patch in 
% square millimeters, a surface triangulation of the patch, the centroid
% location of the patch, and the net dipole of the patch (weighted sum
% of face normals times the constant dipole moment density)
% inputs: 
%         FV: cortex hemisphere surface triangulation
%         normal vectors for each face of triangulation
%         areas in mm^2 for each face of triangulation
%         face index number (index of face in triangular cortex surface)
%              to start patch creation at
%         atarget - target area (minumum) for patch. 
%         side of interest (1==LH, 2==RH)
%         cdmd: constant current dipole moment density to calculate the 
%           net dipole moment of this patch. in nAm/mm^2
% ********************************
%

h.faces = FV.ConnectivityList;
h.vertices = FV.Points;

outer_vertices = [];
total_area = areas(sface);
patch = sface;
for y = 1:3
    outer_vertices = [outer_vertices,h.faces(sface,y)];
end

% iteratively expand patch by outervertices (one vertex at a time)
start = 1;
while total_area < atarget
    disconnected = 0;
     for v = outer_vertices
        if total_area >= atarget
            break
        end
        [ia,~] = find(h.faces==v);
        ia = unique(ia);
        ia(ismember(ia,patch)==1)=[];
        if ~isempty(ia) 
            for f = transpose(ia)
                if total_area >= atarget
                    break
                end
                patch = [patch;f];
                total_area = total_area + areas(f);
                for coord = 1:3
                    if ismember(h.faces(f,coord),outer_vertices) == 0
                        outer_vertices = [outer_vertices,h.faces(f,coord)];
                        start = 0;
                    end
                end
            end 
        else
            disconnected = disconnected + 1;
        end
        outer_vertices(outer_vertices==v)=[];
     end
     if disconnected == 3 && start == 1
         disconnected = "first dipole is disconnected! no patch can be made."
         break
     end
end

% make patch triangulation
Points = [];
ConnectivityList = zeros(length(patch),3);
point_index = 1;
for x = 1:length(patch) % x = patch number
    for c = 1:3
        % h.faces(patch(x),c) = vertex coord index (in h.vertices)
        a = h.vertices(h.faces(patch(x),c),:); % = new vertex coords
        if side == 1
            a(3) = a(3) + 68;
            a(1) = a(1) + 3;
        elseif side == 2
            a(3) = a(3) + 70;
            a(1) = a(1) + 3;
        end
        Points = [Points;a];
        ConnectivityList(x,c) = point_index;
        point_index = point_index + 1;
    end
end
patchfield = triangulation(ConnectivityList,Points);    
    
% calc centroid and netDipole
acc_mass = 0;
acc_area = 0;
b = [0;0;0];
for index = 1:length(patch)
    ctr = normals.node(:,patch(index));
    dipoleM = normals.field(:,patch(index))*areas(patch(index))*cdmd;
    b = b + dipoleM;
    acc_mass = acc_mass + (ctr * areas(patch(index)));
    acc_area = acc_area + areas(patch(index));
end 
a = acc_mass ./ acc_area;
if side == 1
    a(3) = a(3) + 68;
    a(1) = a(1) + 3;
elseif side == 2
    a(3) = a(3) + 70;
    a(1) = a(1) + 3;
end

centroid = a;
netDipole = b;

end



% plot
function stlPlot(v, f, pf, name)
%STLPLOT is an easy way to plot an STL object
%V is the Nx3 array of vertices
%F is the Mx3 array of faces
%NAME is the name of the object, that will be displayed as a title
object.vertices = v;
object.faces = f;
c = [0.8 0.8 1.0];
color = repmat(c,length(f),1);
for k = 1:length(pf)
    change = 1 ;%- (1/(2*length(pf)))*(k-1);
    color(pf(k),:) = [change change 0];
end
patch(object,'FaceVertexCData',       color, ...
         'FaceColor', 'flat',                ...
         'EdgeColor',       'none',        ...
         'FaceLighting',    'gouraud',     ...
         'AmbientStrength', 0.15);
% Add a camera light, and tone down the specular highlighting
camlight('headlight');
material('dull');
% Fix the axes scaling, and set a nice view angle
axis('image');
view([-135 35]);
grid on;
title(name);
end



function [areas] = get_cortex_areas(FV)
    areas = zeros(length(FV.ConnectivityList),1);
    for i = 1:length(areas) % for each face
        face_points = [FV.ConnectivityList(i,1),FV.ConnectivityList(i,2),FV.ConnectivityList(i,3)];
        a = face_points(1);
        pa = FV.Points(a,1:3);
        b = face_points(2);
        pb = FV.Points(b,1:3);
        b = face_points(3);
        pc = FV.Points(b,1:3);
        %calc area from points
        areas(i) = 0.5 * norm(cross(pb-pa,pc-pa));
        %format = "face: %.0f \n    face_points: %.0f %.0f %.0f \n    area: %f \n";
        %fprintf(format, x, face_points, area)
    end
end

function [normals] = get_cortex_normals(FV)

    FVP = FV.Points;
    FVC = FV.ConnectivityList;
    %
    for k = 1:length(FVC)
        %get three points of each triangle
        v1 = FVC(k,1);
        v2 = FVC(k,2);
        v3 = FVC(k,3);
    
        %get coordinates of each point
        a = FVP(v1,:);
        b = FVP(v2,:);
        c = FVP(v3,:);
    
        %calc cross product of two sides
        m = cross(b-a,c-a);
        
        %calc normal vector
        n = m/norm(m);
    
        %calc position by components
        x = (a(1) + b(1) + c(1)) / 3;
        y = (a(2) + b(2) + c(2)) / 3;
        z = (a(3) + b(3) + c(3)) / 3;
    
    %assemble position into vector
        p = [x,y,z];
    
    %put normal vector and position into matrix of row k
        if k == 1
            CDN_n = zeros([length(FVC),3]);
            CDN_p = zeros([length(FVC),3]);
        end
        CDN_n(k,:) = n;
        CDN_p(k,:) = p;
        
    end
  
    normals.node = transpose(CDN_p);
    normals.field = transpose(CDN_n);
    
    figure(1)
    clf
    quiver3(CDN_p(:,1),CDN_p(:,2),CDN_p(:,3),CDN_n(:,1),CDN_n(:,2),CDN_n(:,3),'LineWidth',1);
    hold on
    trimesh(FV)
    % how to make sure normals are pointing correct way??
    
    
end




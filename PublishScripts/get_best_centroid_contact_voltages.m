%% get best centroid contact voltages

% **********************************************************************
% 3/13/21
% Grace Dessert
% **********************************************************************

% for each of three patch resolutions,
% for every direction (by integer degrees) of the centroid dipole current
% source, get the output contact voltages using linear combinations of the
% contact voltages due to the orthogonal basis centroid dipoles, and get
% the r-value (pearson correlation coefficient) of the linear combination
% centroid dipole contact voltages compared to the patch model contact 
% voltages
% The centroid dipole with the highest r-value is determined to be the best
% centroid dipole.

% inputs:
%           CentroidFieldFourDirectionsForEach_LH.mat,centroid  ; CentroidFieldFourDirectionsForEach_RH.mat,centroid
%               3x1 cell array of
%               field of all centroid dipoles (4 per centroid)
%           CentroidModelTrueDirContactVoltages.mat,CentroidModelTrueDirContactVoltages  ;  CentroidModelTrueDirContactVoltagesRH.mat, CentroidModelTrueDirContactVoltages
%               3x1 cell array of
%               112x400 double of all contact voltages of 400 centroid
%               dipoles in the true direction (direction of linear 
%               combination of patch dipoles / patch face normal vectors)
%           CentroidModel$ContactVoltage.mat, CentroidModel$ContactVoltages
%               3x1 cell array of
%               $ = X,Y,Z
%               112x400 double of all contact voltages of 400 centroid
%               dipoles in the x- y- or z- directions
%           PatchContactVoltages.mat,patch_contact_voltages
%               3x1 cell array of
%               112x400 double of all contact voltages of 400 distributed
%               patch models

% outputs:
%           CentroidModelBestDirContactVoltages.mat,bestC_CVs
%               3x1 cell array of
%               112x400 double of all contact voltages of 400 best centroid
%               models. These are the voltages given by the linear
%               combination of contact voltages of the orthogonal basis 
%               dipoles that gives the highest r-value when compared to the
%               patch contact voltages.
%           CentroidModelBestDirCorrelationCoeffs.mat,bestC_r
%               3x1 cell array of
%               1x400 double of the best r-value for every centroid 

%% LH 

% load data
load('PatchContactVoltages_highRes.mat','patch_contact_voltages')
load('CentroidModelXContactVoltages_highRes.mat','CentroidModelXContactVoltages'); 
load('CentroidModelYContactVoltages_highRes.mat','CentroidModelYContactVoltages'); 
load('CentroidModelZContactVoltages_highRes.mat','CentroidModelZContactVoltages'); 
load('Patches_LH_all.mat','patchesLH')

bestC_dirs = cell(3,1);
bestC_CVs = cell(3,1);
bestC_r = cell(3,1);

% for each of 3 resolutions
for r = 1:3
% for each patch, calculate the contact voltages and r-value of every
% direction of centroid dipole (3200 directions total), and save the one
% with the highest r-value.
bestC_dirs{r} = zeros(3,400); % direction of centroid with highest correlation to patch model
bestC_CVs{r} = zeros(112,400); % contact voltages of best centroid model
bestC_r{r} = zeros(1,400);
for patchI = 1:400
    patchI
    contact_voltages_all_dirs = zeros(112,32400);
    dir_corrToPatchModel = zeros(1,32400);
    % load patch model contact voltages to determine correlation
    patch_CVs = patch_contact_voltages{r}(:,patchI);
    % normalze CVs to unit dipole with dipole moment
    dipMoment = patchesLH{r}.centroids.field(:,patchI);
    % load voltages of orthogonal basis vectors of centroid dipole
    centroid_x_voltages = CentroidModelXContactVoltages{r}(:,patchI)/dipMoment(1);
    centroid_y_voltages = CentroidModelYContactVoltages{r}(:,patchI)/dipMoment(2);
    centroid_z_voltages = CentroidModelZContactVoltages{r}(:,patchI)/dipMoment(3);     
    for phi = 1:180
        for theta = 1:180
            ind = (phi-1)*180+theta;
            % get cartesian coordinates from spherical coords
            x = sind(phi)*cosd(theta);
            y = sind(phi)*sind(theta);
            z = cosd(phi);
            % coeffs of linear combination are cartesian coordinates
            cVS = x*centroid_x_voltages + y*centroid_y_voltages + z*centroid_z_voltages;
            dir_corrToPatchModel(ind) = corr2(cVS,patch_CVs);
            contact_voltages_all_dirs(:,ind) = cVS;
            %plot(cVS,patch_CVs,'.')
            %pause(0.01)
        end
    end
    length(find(dir_corrToPatchModel==max(dir_corrToPatchModel)));
    indBest = find(dir_corrToPatchModel==max(dir_corrToPatchModel));
    bestC_dirs{r}(:,patchI) = [x;y;z];
    bestC_r{r}(patchI) = max(dir_corrToPatchModel);
    bestC_CVs{r}(:,patchI) = contact_voltages_all_dirs(:,indBest(1));
    % scale to best dipole moment
    slope = bestC_CVs{r}(:,patchI) \ patch_CVs;
    bestC_CVs{r}(:,patchI) = contact_voltages_all_dirs(:,indBest(1))*slope;
end
end
save('CentroidModelBestDirContactVoltages.mat','bestC_CVs')
save('CentroidModelBestDirCorrelationCoeffs.mat','bestC_r')


%% RH 

% load data
load('PatchContactVoltagesR_highRes.mat','patch_contact_voltages')
load('CentroidModelXContactVoltagesR_highRes.mat','CentroidModelXContactVoltages'); 
load('CentroidModelYContactVoltagesR_highRes.mat','CentroidModelYContactVoltages'); 
load('CentroidModelZContactVoltagesR_highRes.mat','CentroidModelZContactVoltages'); 
load('Patches_RH.mat','patchesRH')

bestC_dirs = cell(3,1);
bestC_CVs = cell(3,1);
bestC_r = cell(3,1);

% for each of 3 resolutions
for r = 1:3
% for each patch, calculate the contact voltages and r-value of every
% direction of centroid dipole (3200 directions total), and save the one
% with the highest r-value.
bestC_dirs{r} = zeros(3,400); % direction of centroid with highest correlation to patch model
bestC_CVs{r} = zeros(112,400); % contact voltages of best centroid model
bestC_r{r} = zeros(1,400);
for patchI = 1:400
    patchI
    contact_voltages_all_dirs = zeros(112,32400);
    dir_corrToPatchModel = zeros(1,32400);
    % load patch model contact voltages to determine correlation
    patch_CVs = patch_contact_voltages{r}(:,patchI);
    % load voltages of orthogonal basis vectors of centroid dipole
    % normalze CVs to unit dipole with dipole moment
    dipMoment = patchesLH{r}.centroids.field(:,patchI);
    % load voltages of orthogonal basis vectors of centroid dipole 
    centroid_x_voltages = CentroidModelXContactVoltages{r}(:,patchI)/dipMoment(1);
    centroid_y_voltages = CentroidModelYContactVoltages{r}(:,patchI)/dipMoment(2);
    centroid_z_voltages = CentroidModelZContactVoltages{r}(:,patchI)/dipMoment(3);
    for phi = 1:180
        for theta = 1:180
            ind = (phi-1)*180+theta;
            % get cartesian coordinates from spherical coords
            x = sind(phi)*cosd(theta);
            y = sind(phi)*sind(theta);
            z = cosd(phi);
            % coeffs of linear combination are cartesian coordinates
            cVS = x*centroid_x_voltages + y*centroid_y_voltages + z*centroid_z_voltages;
            dir_corrToPatchModel(ind) = corr2(cVS,patch_CVs);
            contact_voltages_all_dirs(:,ind) = cVS;
            %plot(cVS,patch_CVs,'.')
            %pause(0.01)
        end
    end
    if length(find(dir_corrToPatchModel==max(dir_corrToPatchModel)))~= 1
        disp('err not one best!')
    end
    indBest = find(dir_corrToPatchModel==max(dir_corrToPatchModel));
    bestC_dirs{r}(:,patchI) = [x;y;z];
    bestC_r{r}(patchI) = max(dir_corrToPatchModel);
    bestC_CVs{r}(:,patchI) = contact_voltages_all_dirs(:,indBest(1));
    % scale to best dipole moment
    slope = bestC_CVs{r}(:,patchI) \ patch_CVs;
    bestC_CVs{r}(:,patchI) = contact_voltages_all_dirs(:,indBest(1))*slope;
end
end
save('CentroidModelBestDirContactVoltagesR.mat','bestC_CVs')
save('CentroidModelBestDirCorrelationCoeffsR.mat','bestC_r')


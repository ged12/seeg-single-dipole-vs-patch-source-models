
function [patch,total_area,patchfield,centroid,netDipole] = make_patch(FV,normals,areas,sface,atarget,side,cdmd)

% ********************************
% make_patch(FV,normals,areas,sface,atarget,side,cdmd) returns an array containing the
% indeces of faces in the cortical surface triangulation of the side of 
% interest (1:left, 2:right), the total area of this patch in 
% square millimeters, a surface triangulation of the patch, the centroid
% location of the patch, and the net dipole of the patch (weighted sum
% of face normals times the constant dipole moment density)
% inputs: 
%         FV: cortex hemisphere surface triangulation
%         normal vectors for each face of triangulation
%         areas in mm^2 for each face of triangulation
%         face index number (index of face in triangular cortex surface)
%              to start patch creation at
%         atarget - target area (minumum) for patch. 
%         side of interest (1==LH, 2==RH)
%         cdmd: constant current dipole moment density to calculate the 
%           net dipole moment of this patch. in nAm/mm^2
% ********************************
%

h.faces = FV.ConnectivityList;
h.vertices = FV.Points;

outer_vertices = [];
total_area = areas(sface);
patch = sface;
for y = 1:3
    outer_vertices = [outer_vertices,h.faces(sface,y)];
end

% iteratively expand patch by outervertices (one vertex at a time)
start = 1;
while total_area < atarget
    disconnected = 0;
     for v = outer_vertices
        if total_area >= atarget
            break
        end
        [ia,~] = find(h.faces==v);
        ia = unique(ia);
        ia(ismember(ia,patch)==1)=[];
        if ~isempty(ia) 
            for f = transpose(ia)
                if total_area >= atarget
                    break
                end
                patch = [patch;f];
                total_area = total_area + areas(f);
                for coord = 1:3
                    if ismember(h.faces(f,coord),outer_vertices) == 0
                        outer_vertices = [outer_vertices,h.faces(f,coord)];
                        start = 0;
                    end
                end
            end 
        else
            disconnected = disconnected + 1;
        end
        outer_vertices(outer_vertices==v)=[];
     end
     if disconnected == 3 && start == 1
         disconnected = "first dipole is disconnected! no patch can be made."
         break
     end
end

% make patch triangulation
Points = [];
ConnectivityList = zeros(length(patch),3);
point_index = 1;
for x = 1:length(patch) % x = patch number
    for c = 1:3
        % h.faces(patch(x),c) = vertex coord index (in h.vertices)
        a = h.vertices(h.faces(patch(x),c),:); % = new vertex coords
        if side == 1
            a(3) = a(3) + 68;
            a(1) = a(1) + 3;
        elseif side == 2
            a(3) = a(3) + 70;
            a(1) = a(1) + 3;
        end
        Points = [Points;a];
        ConnectivityList(x,c) = point_index;
        point_index = point_index + 1;
    end
end
patchfield = triangulation(ConnectivityList,Points);    
    
% calc centroid and netDipole
acc_mass = 0;
acc_area = 0;
b = [0;0;0];
for index = 1:length(patch)
    ctr = normals.node(:,patch(index));
    dipoleM = normals.field(:,patch(index))*areas(patch(index))*cdmd;
    b = b + dipoleM;
    acc_mass = acc_mass + (ctr * areas(patch(index)));
    acc_area = acc_area + areas(patch(index));
end 
a = acc_mass ./ acc_area;
if side == 1
    a(3) = a(3) + 68;
    a(1) = a(1) + 3;
elseif side == 2
    a(3) = a(3) + 70;
    a(1) = a(1) + 3;
end

centroid = a;
netDipole = b;

end


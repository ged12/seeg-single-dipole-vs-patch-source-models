%% test practical linearity of finite element model

% ************************************************************************
% 10/12/20
% Grace Dessert
% ************************************************************************

% for each of three patch resolutions (full res, 145k, and 20k),
% calculate the contact voltages of the centroid dipole in the
% direction of the linear combination of patch face normals using a linear
% combination of the contact voltages of the three orthogonal basis
% centroid dipoles
% 
% then, compare these linear combination contact voltages with the true 
% contact voltages given by the dipole that was originally run in that direction
% to compare them, calculate the maximum voltage difference at any contact
% location


path = '/Volumes/Seagate Backup Plus Drive/disc-signal-grill/CentroidModel/HigherRes/';

%% LH
centroid_linComb_contactVoltages = cell(3,1);
max_error = cell(3,1);
best_dipole_moment_for_simple_centroid_model = cell(3,1);

for r = 1:3
    r
% load all data
load(['centroidDipoles_withBasisDirs_LH_centroidModel_highRes_all.mat'],'centroid')
load(['CentroidModelTrueDirContactVoltages_highRes.mat'],'CentroidModelTrueDirContactVoltages'); 
load(['CentroidModelXContactVoltages_highRes.mat'],'CentroidModelXContactVoltages'); 
load(['CentroidModelYContactVoltages_highRes.mat'],'CentroidModelYContactVoltages'); 
load(['CentroidModelZContactVoltages_highRes.mat'],'CentroidModelZContactVoltages'); 
load(['PatchContactVoltages_highRes.mat'],'patch_contact_voltages')

% initialize output variables
centroid_linComb_contactVoltages{r} = zeros(112,400);
max_error{r} = zeros(1,400);
best_dipole_moment_for_simple_centroid_model{r} = zeros(1,400);


% for each patch, calculate linear combination contact voltages using the
% direction vector of the centroid true direction dipole
for patchI = 1:400
    %patchI
    
% get best dipole moment for centroid model to match patch model
raw_centroid_CVs = CentroidModelTrueDirContactVoltages{r}(:,patchI);
patch_voltages = patch_contact_voltages{r}(:,patchI);
best_dipole_moment_for_simple_centroid_model{r}(patchI) = raw_centroid_CVs \ patch_voltages;

centroid_true_voltages = CentroidModelTrueDirContactVoltages{r}(:,patchI);
true_dir = (centroid{r}.field(:,(patchI-1)*4+1)); % get correct dipole in whole centroid field. four dipoles were run per patch, and first of four is the true direction
centroid_x_voltages = CentroidModelXContactVoltages{r}(:,patchI);
centroid_y_voltages = CentroidModelYContactVoltages{r}(:,patchI);
centroid_z_voltages = CentroidModelZContactVoltages{r}(:,patchI);

% coeffs of linear combination are numbers in true_dir
centroid_linComb_contactVoltages{r}(:,patchI) = true_dir(1)*centroid_x_voltages + true_dir(2)*centroid_y_voltages + true_dir(3)*centroid_z_voltages;

% calculate max error at a contact location
max_error{r}(patchI) = max(abs(centroid_linComb_contactVoltages{r}(:,patchI)-centroid_true_voltages));
end
end
% save data
save(['max_error_centroid_LCtrueDir_contactVoltages_all.mat'],'max_error')
save(['centroid_linCombtrueDir_contactVoltages_all.mat'],'centroid_linComb_contactVoltages')

save(['best_dipole_moment_for_simple_centroid_model_all.mat'],'best_dipole_moment_for_simple_centroid_model')


%% RH
clear max_error
max_error = cell(3,1);

for r = 1:3
    % load all data
    load(['CentroidModelTrueDirContactVoltagesR_highRes.mat'],'CentroidModelTrueDirContactVoltages'); 
    load(['CentroidModelXContactVoltagesR_highRes.mat'],'CentroidModelXContactVoltages'); 
    load(['CentroidModelYContactVoltagesR_highRes.mat'],'CentroidModelYContactVoltages'); 
    load(['CentroidModelZContactVoltagesR_highRes.mat'],'CentroidModelZContactVoltages'); 

    load(['centroidDipoles_withBasisDirs_RH_centroidModel_highRes_all.mat'],'centroid')

    % load patch contact voltages
    load(['PatchContactVoltagesR_highRes.mat'],'patch_contact_voltages')

    % initialize output variables
    centroid_linComb_contactVoltages{r} = zeros(112,400);
    max_error{r} = zeros(1,400);
    best_dipole_moment_for_simple_centroid_model{r} = zeros(1,400);

    % for each patch, calculate linear combination contact voltages using the
    % direction vector of the centroid true direction dipole
    for patchI = 1:400
        % get best dipole moment for centroid model to match patch model
        raw_centroid_CVs = CentroidModelTrueDirContactVoltages{r}(:,patchI);
        patch_voltages = patch_contact_voltages{r}(:,patchI);
        best_dipole_moment_for_simple_centroid_model{r}(patchI) = raw_centroid_CVs \ patch_voltages;

        % load voltages and true centroid dipole direction
        centroid_true_voltages = CentroidModelTrueDirContactVoltages{r}(:,patchI);
        true_dir = (centroid{r}.field(:,(patchI-1)*4+1)); % get correct dipole in whole centroid field. four dipoles were run per patch, and first of four is the true direction
        centroid_x_voltages = CentroidModelXContactVoltages{r}(:,patchI);
        centroid_y_voltages = CentroidModelYContactVoltages{r}(:,patchI);
        centroid_z_voltages = CentroidModelZContactVoltages{r}(:,patchI);

        % coeffs of linear combination are numbers in true_dir
        centroid_linComb_contactVoltages{r}(:,patchI) = (true_dir(1)*centroid_x_voltages + true_dir(2)*centroid_y_voltages + true_dir(3)*centroid_z_voltages);

        % calculate max error at a contact location
        max_error{r}(patchI) = max(abs(centroid_linComb_contactVoltages{r}(:,patchI)-centroid_true_voltages));
    end
    %plot(centroid_true_voltages,centroid_linComb_voltages,'.')
end
save(['max_error_centroid_LCtrueDir_contactVoltages_RH_all.mat'],'max_error')
save(['centroid_linCombtrueDir_contactVoltages_RH_all.mat'],'centroid_linComb_contactVoltages')
save(['best_dipole_moment_for_simple_centroid_modelRH_all.mat'],'best_dipole_moment_for_simple_centroid_model')


% to plot data, see /plot_TestPracticalLinearity_MaxVoltDiff_vs_PatchArea.m

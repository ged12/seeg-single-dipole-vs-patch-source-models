%% get patch model centroid voltages for centroid model patches

% for each of three patch resolutions, for each of 800 patches,
% calculate the whole patch forward solution by taking linear combinations 
% of the patch dipole forward solutions, scaling by the area of each 
% corresponding face on the cortical surface triangluation.
% Then, interpolate the forward solution (grid voltage) onto contact
% locations to get the contact voltages

% ************************************************************************
% 3/16/21
% Grace Dessert
% ************************************************************************

local_or_cluster = 2;
total_num_patches = 400;

% load areas of each face and create standard index area matrix
if local_or_cluster == 2
    load('/hpc/group/wmglab/ged12/cluster/CentroidModel/HigherRes/lh_white_3res_areas.mat','areasLH_all');
    load('/hpc/group/wmglab/ged12/cluster/CentroidModel/HigherRes/rh_white_3res_areas.mat','areasRH_all');
else
    load('/Volumes/Seagate Backup Plus Drive/disc-signal-grill/CentroidModel/PatchResolutionTesting/ErrorProfileData/lh_white_3res_areas.mat','areasLH_all');
    load('/Volumes/Seagate Backup Plus Drive/disc-signal-grill/CentroidModel/PatchResolutionTesting/ErrorProfileData/rh_white_3res_areas.mat','areasRH_all');
end

% load patches and contact locations
if local_or_cluster ==1
    load('/Users/dessertgrace/Documents/project_seeg_discernable_signal_grill_-selected/ContactLocations.mat','tetmesh');
    load('/Volumes/Seagate Backup Plus Drive/disc-signal-grill/CentroidModel/HigherRes/Patches_LH_all.mat','patchesLH')
    load('/Volumes/Seagate Backup Plus Drive/disc-signal-grill/CentroidModel/HigherRes/Patches_RH_all.mat','patchesRH')
else
    load('/hpc/group/wmglab/ged12/cluster/CentroidModel/HigherRes/Patches_LH_all.mat','patchesLH')
    load('/hpc/group/wmglab/ged12/cluster/CentroidModel/HigherRes/Patches_RH_all.mat','patchesRH')
    load('/hpc/group/wmglab/ged12/cluster/ContactLocations.mat','tetmesh');
end
patchesLH_all = patchesLH;
patchesRH_all = patchesRH;

% load grid where forward solution voltages are located
if local_or_cluster == 1 %local
    g = load('/Users/dessertgrace/Documents/project_seeg_discernable_signal_grill_-selected/element-locs.mat');
    grid = transpose(g.scirunfield.node);
else
    g = load('/hpc/group/wmglab/ged12/cluster/element-locs.mat');
    grid = transpose(g.scirunfield.node);
end

% clip grid to set of 6 closest points around each contact, for quick interpolation. 
grid_clip = [];
grid_full_inds = [];
for i = 1:length(tetmesh.node)
    distances = sum(sqrt((grid-tetmesh.node(:,i)').^2),2);
    [B,inds] = sort(distances);
    grid_clip = [grid_clip;grid(inds(1:6),:)];
    grid_full_inds = [grid_full_inds;inds(1:6)];
    grid_clip = unique(grid_clip,'rows','stable');
    grid_full_inds = unique(grid_full_inds,'stable');
end


cdmd = 1; % current dipole moment density is 1nAm/mm^2

%% LH

side = 1;
patch_contact_voltages = cell(3,1);
patch_contact_voltages_byCVs = cell(3,1);
voltage_patchFSs_vis = cell(3,1);
for r = 1:3
    
    areasLH = areasLH_all{r};
    patchesLH = patchesLH_all{r};
    
    % calculate patch forward solutions for each of 400 patches
    % use forward solutions stored in concatenated row files (Cat files) (500 columns of lead field matrix each, 80 files total)
    voltage_patchFSs = cell(1,total_num_patches);
    for p = 1:total_num_patches
        patch = patchesLH.dipoles(p,:);
        patch(patch==0)=[];
        areas_this_patch = areasLH(patch);

        [FS] = patchAdd_FS_res(patch,areas_this_patch,cdmd,side,local_or_cluster,r);
        voltage_patchFSs{p} = FS;
    end

    % concatenate cell array of patch forward solutions into one matrix of patch forward solutions
    voltage_patchFSs_all = zeros(466779,total_num_patches);
    for p=1:total_num_patches
        voltage_patchFSs_all(:,p) = voltage_patchFSs{p};
    end

    %save('PatchForwardSolutions.mat','voltage_patchFSs_all'); 
    %load('PatchForwardSolutions.mat','voltage_patchFSs_all'); 

    % save select patch forward solutions for visualization on SCIRun
    voltage_patchFSs_vis{r} = zeros(466779,9);
    inds = [1,25,50,75,100,125,150,175,200];
    for i=1:9
        p = inds(i);
        voltage_patchFSs_vis{r}(:,i) = voltage_patchFSs_all(:,p);
    end
    save('PatchForwardSolutions_HighRes_Inds1,25,50,75,100,125,150,175,200_forVis.mat','voltage_patchFSs_vis');


    % clip forward solutions to only voltages at clipped grid locations for fast interpolation
    clip_grid_voltage = zeros(length(grid_full_inds),total_num_patches);
    for i = 1:total_num_patches
    clip_grid_voltage(:,i) = voltage_patchFSs{i}(grid_full_inds);
    end

    % interpolate clip grid voltages onto contact locations
    contLocs = tetmesh.node;
    contact_voltage_avg = zeros(length(tetmesh.node),total_num_patches); % each column is interpolated contact voltage given that one dipole FS (using voltage at clipped grid nodes only)
    for i = 1:total_num_patches
        % create interpolant using clipped grid locations and voltages at clipped grid locations
        F = scatteredInterpolant(grid_clip(:,1),grid_clip(:,2),grid_clip(:,3),clip_grid_voltage(:,i));
        % interpolate voltage onto contact locations to get voltage at contact locations
        contact_voltage_avg(:,i) = F(contLocs(1,:)',contLocs(2,:)',contLocs(3,:)');
    end
    patch_contact_voltages{r} = contact_voltage_avg;

    % get concact voltages by simple linear combination of dipole CVs
    if r == 3  % for 20k resolution, we cannot get LC of CVs bc I did not save CVs for face dipoles
        contact_voltage_CVs = [];
    else
        contact_voltage_CVs = zeros(112,total_num_patches);
        for i = 1:total_num_patches
            patch = patchesLH.dipoles(i,:);
            patch(patch==0)=[];
            areas_this_patch = areasLH(patch);
            CVs = patchAdd_ContactVoltages_res(patch,areas_this_patch,cdmd,side,local_or_cluster,r);
            contact_voltage_CVs(:,i) = CVs; 
        end
    end
    patch_contact_voltages_byCVs{r} = contact_voltage_CVs;
end

% save  patch contact voltages
save('PatchContactVoltages_highRes.mat','patch_contact_voltages')
save('PatchContactVoltages_byCVs_highRes.mat','patch_contact_voltages_byCVs')



%% RH
patch_contact_voltages = cell(3,1);
patch_contact_voltages_byCVs = cell(3,1);
voltage_patchFSs_vis = cell(3,1);

side = 2;
    
for r = 1:3
    areasRH = areasRH_all{r};
    patchesRH = patchesRH_all{r};
    
    % calculate patch forward solutions for each of 400 patches
    % use forward solutions stored in concatenated row files (Cat files) (500 columns of lead field matrix each, 80 files total)
    voltage_patchFSs = cell(1,total_num_patches);
    for p = 1:total_num_patches
        patch = patchesRH.dipoles(p,:);
        patch(patch==0)=[];
        areas_this_patch = areasRH(patch);

        [FS] = patchAdd_FS_res(patch,areas_this_patch,cdmd,side,local_or_cluster,r);
        voltage_patchFSs{p} = FS;
    end

    % concatenate cell array of patch forward solutions into one matrix of patch forward solutions
    voltage_patchFSs_all = zeros(466779,total_num_patches);
    for p=1:total_num_patches
        voltage_patchFSs_all(:,p) = voltage_patchFSs{p};
    end

    % save select patch forward solutions for visualization on SCIRun
    voltage_patchFSs_vis{r} = zeros(466779,9);
    inds = [1,25,50,75,100,125,150,175,200];
    for i=1:9
        p = inds(i);
        voltage_patchFSs_vis{r}(:,i) = voltage_patchFSs_all(:,p);
    end
    save('PatchForwardSolutionsR_HighRes_Inds1,25,50,75,100,125,150,175,200_forVis.mat','voltage_patchFSs_vis');


    % clip forward solutions to only voltages at clipped grid locations for fast interpolation
    clip_grid_voltage = zeros(length(grid_full_inds),total_num_patches);
    for i = 1:total_num_patches
    clip_grid_voltage(:,i) = voltage_patchFSs{i}(grid_full_inds);
    end

    % interpolate clip grid voltages onto contact locations
    contLocs = tetmesh.node;
    contact_voltage_avg = zeros(length(tetmesh.node),total_num_patches); % each column is interpolated contact voltage given that one dipole FS (using voltage at clipped grid nodes only)
    for i = 1:total_num_patches
        % create interpolant using clipped grid locations and voltages at clipped grid locations
        F = scatteredInterpolant(grid_clip(:,1),grid_clip(:,2),grid_clip(:,3),clip_grid_voltage(:,i));
        % interpolate voltage onto contact locations to get voltage at contact locations
        contact_voltage_avg(:,i) = F(contLocs(1,:)',contLocs(2,:)',contLocs(3,:)');
    end
    patch_contact_voltages{r} = contact_voltage_avg;

    % get concact voltages by simple linear combination of dipole CVs
    if r == 3  % for 20k resolution, we cannot get LC of CVs bc I did not save CVs for face dipoles
        contact_voltage_CVs = [];
    else
        contact_voltage_CVs = zeros(112,total_num_patches);
        for i = 1:total_num_patches
            patch = patchesRH.dipoles(i,:);
            patch(patch==0)=[];
            areas_this_patch = areasRH(patch);
            CVs = patchAdd_ContactVoltages_res(patch,areas_this_patch,cdmd,side,local_or_cluster,r);
            contact_voltage_CVs(:,i) = CVs; 
        end
    end
    patch_contact_voltages_byCVs{r} = contact_voltage_CVs;
    
end

% save  patch contact voltages
save('PatchContactVoltagesR_highRes.mat','patch_contact_voltages')
save('PatchContactVoltagesR_byCVs_highRes.mat','patch_contact_voltages_byCVs')



function [FS] = patchAdd_FS_res(linearPatchDipoles,areas_this_patch,cdmd,side,local_or_cluster,resolution)

FS = zeros(466779,1);
for d = 1:length(linearPatchDipoles)
    
    dInd = linearPatchDipoles(d);
    
    if local_or_cluster == 2
        if resolution ==1 % full res
            if side == 1
                load(['/work/ged12/CentroidModel/PatchResolutionTesting/ErrorProfile/Data/FS/HeadDipoleVoltage_fullResDipole' num2str(dInd) '.mat'],'scirunmatrix');
            else
                load(['/work/ged12/CentroidModel/PatchResolutionTesting/ErrorProfile/DataR/FS/HeadDipoleVoltage_fullResDipoleR' num2str(dInd) '.mat'],'scirunmatrix');
            end
        elseif resolution == 2  % 145k res
            if side == 1
                load(['/work/ged12/CentroidModel/HigherRes/Data/DipoleFaces/FS/HeadDipoleVoltage_' num2str(dInd) '.mat'],'scirunmatrix');
            else
                load(['/work/ged12/CentroidModel/HigherRes/Data/DipoleFaces/FS/HeadDipoleVoltageR_' num2str(dInd) '.mat'],'scirunmatrix');
            end
        elseif resolution == 3  % 20k res
            if side == 1
                load(['/work/ged12/DipoleData/T0/HeadDipoleVoltageSampledAtRecLoc' num2str(dInd) '.mat'],'scirunmatrix');
            else
                load(['/work/ged12/DipoleDataR/T0/HeadDipoleVoltageSampledAtRecLocR' num2str(dInd) '.mat'],'scirunmatrix');
            end
        else
            disp('ERR: invalid resolution index')
        end    
    else
        disp('ERR: no data local')
    end
    FS = FS + (scirunmatrix*areas_this_patch(d)*cdmd); % add this FS to patch FS, scaling by area and cdmd
    
end

end

function [CVs] = patchAdd_ContactVoltages_res(linearPatchDipoles,areas_this_patch,cdmd,side,local_or_cluster,resolution)

CVs = zeros(112,1);
for d = 1:length(linearPatchDipoles)
    
    dInd = linearPatchDipoles(d);
    
    if local_or_cluster == 2
        if resolution == 1  % full res
            if side == 1
                load(['/work/ged12/CentroidModel/PatchResolutionTesting/ErrorProfile/Data/ContactLocs/HeadDipoleVoltage_ContactVoltage_fullResDipole' num2str(dInd) '.mat'],'scirunmatrix');
            else
                load(['/work/ged12/CentroidModel/PatchResolutionTesting/ErrorProfile/DataR/ContactLocs/HeadDipoleVoltage_ContactVoltage_fullResDipoleR' num2str(dInd) '.mat'],'scirunmatrix');
            end
        elseif resolution == 2 % 145k
            if side == 1
                load(['/work/ged12/CentroidModel/HigherRes/Data/DipoleFaces/ContactVoltage/HeadDipoleVoltage_ContactLocs_' num2str(dInd) '.mat'],'scirunmatrix');
            else
                load(['/work/ged12/CentroidModel/HigherRes/Data/DipoleFaces/ContactVoltage/HeadDipoleVoltageR_ContactLocs_' num2str(dInd) '.mat'],'scirunmatrix');
            end
        else
            disp('err invalid resolution')
        end
    else
        disp('err no data local')
    end
    CVs = CVs + (scirunmatrix*areas_this_patch(d)*cdmd); % add these CVs to patch CVs, scaling by area and cdmd
    
end
end





